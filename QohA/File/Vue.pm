package QohA::File::Vue;

use Modern::Perl;
use Moo;

extends 'QohA::File';

use File::Slurp qw[read_file];

sub run_checks {
    my ($self, $cnt) = @_;

    my $r = $self->check_tidiness;
    $self->SUPER::add_to_report('Tidiness', $r);

    return $self->SUPER::run_checks($cnt);
}

sub check_tidiness {
    my ($self) = @_;

    return q{} unless -e $self->path;

    my $absf = File::Spec->rel2abs($self->path);
    my @lines = read_file($absf);
    my $tidy = qx{yarn --silent run prettier --trailing-comma es5 --semi false --arrow-parens avoid $absf};
    my $content = read_file $absf;

    return ["File is not tidy, please run `yarn run prettier --trailing-comma es5 --semi false --arrow-parens avoid --write $absf`"] if $content ne $tidy;
}

1;

=head1 AUTHOR

Jonathan Druart <jonathan.druart@bugs.koha-community.org>

=head1 COPYRIGHT AND LICENSE

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007

=cut
